<p style="text-align: center;">
  <a href="https://www.rosatom.ru/" target="blank"><img src="https://seeklogo.com/images/R/rosatom-logo-3EA6526E29-seeklogo.com.png" width="100" alt="Rosatom Logo" /></a>
</p>

## Description

Android Rosatom-helper application with DialogFlow integration.
Was done with love by BORN2CODE on the [leaders-of-digital](https://leadersofdigital.ru/) contest.

## Installation
Simple gradle installation

## Run
Run it with Android studio