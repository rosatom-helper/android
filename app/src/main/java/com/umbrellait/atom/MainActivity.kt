package com.umbrellait.atom

import android.Manifest
import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.api.gax.core.FixedCredentialsProvider
import com.google.api.gax.rpc.ApiStreamObserver
import com.google.auth.oauth2.GoogleCredentials
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.dialogflow.v2.*
import com.google.cloud.speech.v1.*
import com.google.protobuf.ByteString
import com.umbrellait.atom.databinding.ActivityMainBinding
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean


class MainActivity : AppCompatActivity() {
    private  var  _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

//    companion object {
//        private val PERMISSIONS = arrayOf(Manifest.permission.RECORD_AUDIO)
//        private const val REQUEST_RECORD_AUDIO_PERMISSION = 200
//    }

//    private var mPermissionToRecord = false
//    private var mAudioEmitter: AudioEmitter? = null
//
//    private val mSpeechClient by lazy {
//        // NOTE: The line below uses an embedded credential (res/raw/sa.json).
//        //       You should not package a credential with real application.
//        //       Instead, you should get a credential securely from a server.
//        applicationContext.resources.openRawResource(R.raw.key).use {
//            SpeechClient.create(SpeechSettings.newBuilder()
//                .setCredentialsProvider { GoogleCredentials.fromStream(it) }
//                .build())
//        }
//    }

    val uuid = UUID.randomUUID().toString()
    lateinit var sessionsClient: SessionsClient
    lateinit var session: SessionName

    private val isRecording = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.streamButtom.setOnClickListener {
            if(isRecording) {

            } else {

            }
        }

        initChatV2()

        makeRequest()
    }

    fun initChatV2() {
        try {
            val stream =
                resources.openRawResource(R.raw.nomadi_vehicle_296810_4cb4f7ecf685)//key)

            val credentials = GoogleCredentials.fromStream(stream)
            val projectId = (credentials as ServiceAccountCredentials).projectId

            val settingsBuilder: SessionsSettings.Builder = SessionsSettings.newBuilder()
            val sessionsSettings: SessionsSettings = settingsBuilder
                .setCredentialsProvider(
                    FixedCredentialsProvider
                        .create(
                            credentials
                        )
                )
                .build()

            sessionsClient = SessionsClient.create(sessionsSettings)
            session = SessionName.of(projectId, uuid) // uuid = UUID.randomUUID().toString()

            Log.e("TAG","Session :${session.session}")
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    @SuppressLint("StaticFieldLeak")
    fun makeRequest() {
            val queryInput = QueryInput
                .newBuilder()
                .setText(
                    TextInput
                        .newBuilder()
                        .setText("Привет")
                        .setLanguageCode("ru-RU"))
                .build()

            val detectIntentRequest: DetectIntentRequest = DetectIntentRequest.newBuilder()
                .setSession(session.toString())
                .setQueryInput(queryInput)
                .build()

            val result = sessionsClient.detectIntent(detectIntentRequest)
            Log.e("TAG","Result :${result}")

    }
}